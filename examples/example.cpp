#include <phero.h>

Phero::DigitalRead dr("dr", 4);
Phero::Enigma enigma("enig", dr);
Phero::Device device(enigma);

void setup() { device.begin(PHERO_CALLBACK(device)); }
void loop() { device.loop(); }
