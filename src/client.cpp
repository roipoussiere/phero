#include "client.h"


WiFiClient wifi_client;
PubSubClient mqtt_client(wifi_client);


namespace Phero {

	MqttClient::MqttClient() {
		this->client_name_ = "_" + std::string(BOARD_NAME);
	}

	void MqttClient::setupWifi() {
		#ifdef TARGET_ESP32
		pinMode(LED_BUILTIN, OUTPUT);
		#endif
		this->blink(1);

		delay(10);
		LOG("Connecting to " + std::string(WIFI_SSID) + "...");

		#ifdef TARGET_ESP32
		WiFi.mode(WIFI_STA);
		#endif

		WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

		while (WiFi.status() != WL_CONNECTED) {
			delay(100);
		}

		this->ip_ = std::string(WiFi.localIP().toString().c_str());
		LOG("Connected to wifi network via IP " + this->ip_ + ".\n");

		this->blink(2);
	}

	void MqttClient::subscribe(std::string topic) {
		LOG("subscribing to " + topic);
		mqtt_client.subscribe(topic.c_str());
	}

	void MqttClient::publish(std::string topic, std::string payload) {
		LOG("> " + topic + ": " + payload);
		mqtt_client.publish(topic.c_str(), payload.c_str());
	}

	void MqttClient::reconnect() {
		uint8_t n_attempt = 0;

		while ( ! mqtt_client.connected() ) {
			n_attempt++;
			LOG("Attempting MQTT connection to " + std::string(BROKER_HOST) + ":" + std::to_string(BROKER_PORT) + "..." );
			if (mqtt_client.connect(this->client_name_.c_str())) {
				LOG("Connected after " + std::to_string(n_attempt) + " attempt(s).");
				this->blink(3);
				this->onConnected();
			} else {
				if (n_attempt > MQTT_ATTEMPS) {
					LOG("Attempt n°" + std::to_string(n_attempt) + " failed, rebooting...");
					this->blink(5);

					ESP.restart();
				}
				LOG("Attempt n°" + std::to_string(n_attempt)
					+ " failed (rc=" + std::to_string(mqtt_client.state())
					+ ") - trying again in " + std::to_string(MQTT_RECONNECT_DELAY) + " seconds...\n");
				delay(MQTT_RECONNECT_DELAY * 1000);
			}
		}
	}

	void MqttClient::callback(char* topic, uint8_t* payload_bytes, uint8_t payload_length) {
		char payload[payload_length + 1] = "";
		strncpy(payload, (char*)payload_bytes, payload_length);
		payload[payload_length] = '\0';

		LOG("< " + std::string(topic) + ": " + payload);
		this->onReceive(std::string(topic), std::string(payload));
	}

	void MqttClient::begin(std::function<void(char*, uint8_t*, uint8_t)> callback) {
		#ifdef DEBUG
		DEBUG_SERIAL.begin(SERIAL_BAUDS);
		#endif

		LOG("compiled at " + std::string(BUILD_DATE));
		LOG("commit " + std::string(COMMIT_HASH) + " (" + std::string(COMMIT_DATE) + ")");

		this->setupWifi();
		mqtt_client.setServer(BROKER_HOST, BROKER_PORT);
		mqtt_client.setCallback(callback);
		this->reconnect();
	}

	void MqttClient::loop() {
		if ( ! mqtt_client.connected() ) {
			LOG("Lost mqtt connexion, reconnecting...");
			this->reconnect();
		}
		mqtt_client.loop();
	}

	void MqttClient::blink(uint8_t times) {
		#ifdef TARGET_ESP32
		delay(BLINK_DELAY * 3);
		for (uint8_t i=0; i<times; i++) {
			digitalWrite(LED_BUILTIN, HIGH);
			delay(BLINK_DELAY);
			digitalWrite(LED_BUILTIN, LOW);
			delay(BLINK_DELAY);
		}
		delay(BLINK_DELAY * 3);
		#endif
	}

}
