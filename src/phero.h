#include "phero_config.h"

#include <string>
#include "client.h"
#include "enigma.h"
#include "device.h"

// always include digital_read and digital_write modules for convenience
#include "modules/digital_read/digital_read.h"
#include "modules/digital_write/digital_write.h"

#ifdef PHERO_HYSTERESIS
#include "modules/hysteresis/hysteresis.h"
#endif

#ifdef PHERO_BLINK
#include "modules/blink/blink.h"
#endif

// dac driver is not supported on esp8266
#if (defined PHERO_DAC && defined TARGET_ESP32)
#include "modules/dac/dac.h"
#endif

#ifdef PHERO_RC522
#include "modules/rc522/rc522.h"
#endif

#ifdef PHERO_74HC595
#include "modules/74hc595/74hc595.h"
#endif

#ifdef PHERO_THERMAL_PRINTER
#include "modules/thermal_printer/thermal_printer.h"
#endif

#ifdef PHERO_DFPLAYER
#include "modules/df_player/df_player.h"
#endif

#ifdef PHERO_NEOPIXEL
#include "modules/neopixel/neopixel.h"
#endif

#ifdef PHERO_MCP4725
#include "modules/mcp4725/mcp4725.h"
#endif
