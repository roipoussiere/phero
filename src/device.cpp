#include "device.h"


namespace Phero {

	Enigma* Device::getEnigma(std::string enigma_id) {
		return this->enigmas_[enigma_id];
	}

	void Device::addEnigma(Enigma& enigma) {
		this->enigmas_[enigma.getEnigmaId()] = &enigma;

		for(auto module : enigma.getModules()) {
			this->modules_[module.first] = module.second;
		}
	}

	ModuleInterface* Device::getModule(std::string module_id) {
		return this->modules_[module_id];
	}

	std::map<std::string, ModuleInterface*> Device::getModules() {
		return this->modules_;
	}

	void Device::begin(std::function<void(char*, uint8_t*, uint8_t)> callback) {
		std::string module_response;
		std::string modules_str;
		char response_type;

		MqttClient::begin(callback);
		for ( auto module: this->modules_) {
			modules_str += module.first + ",";
			this->subscribe(module.second->getQueryTopic());
			module_response = module.second->begin();

			if (module_response != "") {
				response_type = module_response[0];
				module_response = module_response.substr(1);
				if (response_type == 'w') {
					this->publish(module.second->getWarningTopic(), module_response);
				} else if (response_type == 'e') {
					this->publish(module.second->getErrorTopic(), module_response);
				}
			}
		}
		modules_str = modules_str.substr(0, modules_str.length() - 1);
		this->publish(this->getInfoTopic(INFO_READY), modules_str);
	}

	void Device::loop() {
		std::string state;
		for ( auto module: this->modules_) {
			state = module.second->checkState();
			if ( state != "" ) {
				this->publish(module.second->getInfoTopic(), state);
			}
		}
		MqttClient::loop();
	}

	void Device::onConnected() {
		LOG("connected");

		this->publish(this->getInfoTopic(INFO_START), \
			std::to_string(BUILD_TIMESTAMP) + "-" + std::string(COMMIT_HASH));

		if (this->enigmas_.size() == 0) {
			this->publish(this->getWarningTopic(WARN_NO_ENIGMA), "");
		}
	}

	void Device::onReceive(std::string topic, std::string payload) {
		this->last_mqtt_topic_ = topic;
		this->last_mqtt_payload_ = payload;

		std::string type = this->getTypeFromTopic(topic);
		std::string source = this->getSourceFromTopic(topic);
		std::string module = this->getModuleFromTopic(topic);

		if (type == MQTT_TOPIC_DEVICE_QUERY) {
			this->triggerDeviceModule(module, payload);
		} else if (type == MQTT_TOPIC_MODULE_QUERY) {
			for (auto const& pair : this->modules_) {
				if (pair.first == module) {
					LOG("Triggering module " + module + " of enigma " + source + ": " + payload);
					pair.second->trigger(payload);
				}
			}
		}
	}

	void Device::checkModulesState() {
		std::string module_state = "";
		for (auto const& pair : this->modules_) {
			module_state = pair.second->checkState();
			if (module_state != "") {
				std::string topic = std::string(MQTT_TOPIC_MODULE_INFO) \
					+ "/" + pair.second->getEnigmaId() \
					+ "/" + pair.first;
				this->publish(topic, module_state);
			}
		}
	}

	void Device::triggerDeviceModule(std::string module, std::string payload) {
		LOG("Builtin module " + module + " not yet implemented.");
	}

	std::string Device::getTypeFromTopic(std::string topic) {
		return topic.substr(0, topic.find('/'));
	}

	std::string Device::getSourceFromTopic(std::string topic) {
		std::size_t source_pos = topic.find('/') + 1;
		return topic.substr(source_pos, topic.find('/', source_pos) - source_pos);
	}

	std::string Device::getModuleFromTopic(std::string topic) {
		std::size_t source_pos = topic.find('/') + 1;
		return topic.substr(topic.find('/', source_pos) + 1);
	}

}
