#pragma once

#include <string>
#include <Arduino.h>

#ifdef TARGET_ESP8266
#include <ESP8266WiFi.h>
#else
#include <WiFi.h>
#endif

#include <PubSubClient.h>
#include "phero_config.h"

#define MQTT_RECONNECT_DELAY 3
#define MQTT_ATTEMPS         5
#define BLINK_DELAY          100
#define DEBUG_SERIAL         Serial

#ifndef LED_BUILTIN
#define LED_BUILTIN          2
#endif

#define PHERO_CALLBACK(dev)  [](char* to, uint8_t* pl, uint8_t len) { dev.callback(to, pl, len); }
#define CALLBACK_SIGNATURE   std::function<void(char*, uint8_t*, uint8_t)> callback

#ifndef LOG
#ifdef DEBUG
#define LOG(msg) DEBUG_SERIAL.println((std::string("") + msg).c_str())
#else
#define LOG(msg)
#endif
#endif


namespace Phero {

	// MQTT client. Must be defined *outside* the setup() and loop() functions.
	class MqttClient {

		public:
			MqttClient();

			std::string getClientName() { return this->client_name_; }
			virtual void onReceive(std::string topic, std::string payload) = 0;
			virtual void onConnected() = 0;
			void setupWifi();
			void begin(CALLBACK_SIGNATURE);
			void loop();
			void callback(char* topic, uint8_t* payload_bytes, uint8_t payload_length);
			void subscribe(std::string topic);
			void publish(std::string topic, std::string payload);

		protected:
			std::string client_name_;
			std::string ip_;

			void reconnect();
			void blink(uint8_t times);
	};

}
