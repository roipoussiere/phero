// Define all usable board pins, mainly used for unit tests
// and eventually to check input data in the future.
#include <Arduino.h>


#ifdef TARGET_ESP32
// esp32 pinouts, except:
// - pins 1 and 3: it's RX/TX and so used by test runner for serial comm);
// - pins 36, 39, 34, 35: can’t be used as outputs neither input pulldown.
//                      ┌─┴───┴───┴───┴───┴───┴───┴───┴───┴───┴───┴───┴───┴───┴───┴────┐ R
const uint8_t pins[] = {  23, 22,/*1, 3,*/21, 19, 18, 5 , 17, 16, 4 , 2 , 15, //gnd,3v3├┐
                        /*en, 36, 39, 34, 35*/32, 33, 25, 26, 27, 14, 12, 13};//gnd,vin├┘
//                      └─┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬────┘ L
#endif

// Note 1: Unit tests show that on some boards,
//         pin 2 and 5 mode's can not be set to pull-up and poll-down, respectively.
// Note 2: Pin 2 must be reserved for the mqtt client (built-in led).

// esp01 pinouts, except:
// - pins 1 and 3: it's RX/TX and so used by test runner for serial comm).
#ifdef TARGET_ESP8266
const uint8_t pins[] = { 0, 2 };
#endif
