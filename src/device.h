#pragma once

#include <map>
#include <initializer_list>

#include "client.h"
#include "modules/module_interface.h"
#include "enigma.h"

#define LOOP_PERIOD_MS 50
#define MQTT_RECONNECT_DELAY 3

#define MQTT_TOPIC_DEVICE_QUERY    "dq"
#define MQTT_TOPIC_DEVICE_RESPONSE "dr"
#define MQTT_TOPIC_DEVICE_INFO     "di"
#define MQTT_TOPIC_DEVICE_WARNING  "dw"
#define MQTT_TOPIC_DEVICE_ERROR    "de"

//                                ┌──────────────────────┬─────────────────────────────────┐
//                                │ trigger condition    │ payload content                 │
//                                ├──────────────────────┼─────────────────────────────────┤
#define INFO_START        "hi" // │ device starts        │ build timestamp and commit hash │
#define INFO_READY        "rd" // │ modules loaded       │ list of modules identifier      │
#define WARN_NO_ENIGMA    "ne" // │ device has no enigma │ empty                           │
// TODO:                          ├──────────────────────┼─────────────────────────────────┤
#define WARN_PIN_CONFLICT "pc" // │ pins conflict        │ conflicting pins number         │
//                                └──────────────────────┴─────────────────────────────────┘


namespace Phero {

	// Phero Device, associated to a physical board.
	// Device-related MQTT messages are in the form:
	// <device action>/<device id>/<core module id>
	// ex: ds/Dem/hi
	class Device : public MqttClient {

		public:
			template<class... Ts>
			Device(Ts&&... enigmas) : MqttClient() {
				this->last_time_ = millis();
				this->last_mqtt_topic_ = "";
				this->last_mqtt_payload_ = "";

				std::initializer_list<Enigma*> _enigmas = { &enigmas... };
				for(auto enigma : _enigmas) {
					this->addEnigma(*enigma);
				}
			}

			void onReceive(std::string topic, std::string payload);
			void begin(std::function<void(char*, uint8_t*, uint8_t)> callback);
			void loop();
			std::string getLastMqttTopic()   { return this->last_mqtt_topic_; }
			std::string getLastMqttPayload() { return this->last_mqtt_payload_; }
			void onConnected();
			void addEnigma(Enigma& enigma);
			Enigma* getEnigma(std::string enigma_id);
			ModuleInterface* getModule(std::string module_id);
			std::map<std::string, ModuleInterface*> getModules();

			std::string getQueryTopic   (std::string key) { return std::string(MQTT_TOPIC_DEVICE_QUERY   ) + this->getTopicSuffix(key); }
			std::string getResponseTopic(std::string key) { return std::string(MQTT_TOPIC_DEVICE_RESPONSE) + this->getTopicSuffix(key); }
			std::string getInfoTopic    (std::string key) { return std::string(MQTT_TOPIC_DEVICE_INFO    ) + this->getTopicSuffix(key); }
			std::string getWarningTopic (std::string key) { return std::string(MQTT_TOPIC_DEVICE_WARNING ) + this->getTopicSuffix(key); }
			std::string getErrorTopic   (std::string key) { return std::string(MQTT_TOPIC_DEVICE_ERROR   ) + this->getTopicSuffix(key); }

		protected:
			unsigned long last_time_;
			std::string last_mqtt_topic_;
			std::string last_mqtt_payload_;
			std::map<std::string, Enigma*> enigmas_;
			std::map<std::string, ModuleInterface*> modules_;

			std::string getTopicSuffix  (std::string key) { return "/" + this->client_name_ + "/" + key; }
			void checkModulesState();
			void triggerDeviceModule(std::string module, std::string payload);
			std::string getTypeFromTopic(std::string topic);
			std::string getSourceFromTopic(std::string topic);
			std::string getModuleFromTopic(std::string topic);

	};

}
