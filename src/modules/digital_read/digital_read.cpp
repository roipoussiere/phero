#include "modules/digital_read/digital_read.h"


namespace Phero {

	DigitalRead::DigitalRead(std::string module_id, uint8_t pin_in, uint8_t mode): ModuleInterface(module_id) {
		this->pin_in_ = pin_in;
		this->mode_ = mode;
	}

	void DigitalRead::init(std::string module_id, uint8_t pin_in, uint8_t mode) {
		this->module_id_ = module_id;
		this->pin_in_ = pin_in;
		this->mode_ = mode;
	}

	std::string DigitalRead::begin() {
		pinMode(this->pin_in_, this->mode_);
		this->last_pin_state_ = digitalRead(this->pin_in_);
		return "";
	}

	void DigitalRead::trigger(std::string payload) {
		// TODO: send mqtt warning (pin_in has no trigger)
	}

	std::string DigitalRead::checkState() {
		bool pin_is_up = digitalRead(this->pin_in_);

		if (pin_is_up && ! this->last_pin_state_) {
			this->last_pin_state_ = HIGH;
			delay(DEBOUNCE_DELAY);
			return MQTT_PAYLOAD_HIGH;
		} else if (! pin_is_up && this->last_pin_state_) {
			this->last_pin_state_ = LOW;
			delay(DEBOUNCE_DELAY);
			return MQTT_PAYLOAD_LOW;
		}
		return "";
	}

}
