#pragma once

#include <Arduino.h>
#include "modules/module_interface.h"

#define MQTT_PAYLOAD_HIGH "high"
#define MQTT_PAYLOAD_LOW  "low"
#define DEBOUNCE_DELAY 500


namespace Phero {

	class DigitalRead : public ModuleInterface {

		public:
			DigitalRead(std::string module_id = "unknown_d_read_module", uint8_t pin_in = 32, uint8_t mode = INPUT_PULLUP);
			void init(std::string module_id, uint8_t pin_in, uint8_t mode = INPUT_PULLUP);

			std::string begin();
			void trigger(std::string payload);
			std::string checkState();

		protected:
			uint8_t pin_in_;
			uint8_t mode_;
			bool last_pin_state_;

	};

}
