#pragma once

#include "modules/module_interface.h"
#include <string>

namespace Phero {

	class Blink : public ModuleInterface {

		public:
			Blink(std::string module_id, uint8_t pin_blink, float initial_freq = 0, bool start_state = LOW);
			std::string begin();
			void trigger(std::string payload);
			std::string checkState();

		protected:
			uint8_t pin_blink_;
			float initial_freq_;
			bool start_state_;

	};

}
