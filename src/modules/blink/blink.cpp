#ifdef PHERO_BLINK

#include "74hc595/blink/blink.h"

namespace Phero {

	Blink::Blink(std::string module_id, uint8_t pin_blink, float initial_freq, bool start_state) : ModuleInterface(module_id) {
		this->pin_blink_ = pin_blink;
		this->initial_freq_ = initial_freq;
		this->start_state_ = start_state;
	}

	std::string Blink::begin() {
		pinMode(this->pin_blink_, OUTPUT);
		return "";
	}

	void Blink::trigger(std::string payload) {
		float blink_frequency;

		if (sscanf(payload.c_str(), "%f", &blink_frequency) == 1) {
			LOG("blink frequency: " + std::to_string(blink_frequency));
		} else {
			// TODO: send mqtt warning (unrecognized payload)
		}}

	string Blink::checkState() {
		return "";
	}

}

#endif
