#pragma once

#include <string>
#include <vector>
#include <Arduino.h>

#define MQTT_TOPIC_MODULE_QUERY    "mq"
#define MQTT_TOPIC_MODULE_RESPONSE "mr"
#define MQTT_TOPIC_MODULE_INFO     "mi"
#define MQTT_TOPIC_MODULE_WARNING  "mw"
#define MQTT_TOPIC_MODULE_ERROR    "me"

#define DEBUG_SERIAL Serial

#ifndef LOG
#ifdef DEBUG
#define LOG(msg) DEBUG_SERIAL.println((std::string("") + msg).c_str())
#else
#define LOG(msg)
#endif
#endif


namespace Phero {

	// Phero module interface, from which all Phero modules inherits.
	// Enigma-related MQTT messages are in the form:
	// <enigma action>/<enigma id>/<module id>
	// ex: es/enig_test/btn, er/enig_test/led
	class ModuleInterface {

		public:
			ModuleInterface(std::string module_id) : module_id_(module_id) {}
			ModuleInterface() : module_id_("unknown_module") {}

			std::string getModuleId() { return this->module_id_; }
			std::string getEnigmaId() { return this->enigma_id_; }

			std::string getQueryTopic()    { return std::string(MQTT_TOPIC_MODULE_QUERY   ) + this->getTopicSuffix(); }
			std::string getResponseTopic() { return std::string(MQTT_TOPIC_MODULE_RESPONSE) + this->getTopicSuffix(); }
			std::string getInfoTopic()     { return std::string(MQTT_TOPIC_MODULE_INFO    ) + this->getTopicSuffix(); }
			std::string getWarningTopic()  { return std::string(MQTT_TOPIC_MODULE_WARNING ) + this->getTopicSuffix(); }
			std::string getErrorTopic()    { return std::string(MQTT_TOPIC_MODULE_ERROR   ) + this->getTopicSuffix(); }

			void setEnigma(std::string enigma_id) { this->enigma_id_ = enigma_id; }

			virtual std::string begin() = 0;
			virtual void trigger(std::string payload) = 0;
			virtual std::string checkState() = 0;

		protected:
			std::string module_id_;
			std::string enigma_id_;

			std::string getTopicSuffix()   { return "/" + this->enigma_id_ + "/" + this->module_id_; }

	};

}
