#pragma once

#include "modules/module_interface.h"
#include "Adafruit_Thermal.h"

namespace Phero {

	// Phero module for thermal printer
	// dependencies: adafruit/Adafruit Thermal Printer Library @ 1.4.0
	class ThermalPrinter : public Phero::ModuleInterface {

		public:
			ThermalPrinter(std::string module_id);
			std::string begin();
			void trigger(std::string payload);
			std::string checkState();

		protected:
			Adafruit_Thermal printer_;

	};

}
