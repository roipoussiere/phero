#ifdef PHERO_THERMAL_PRINTER

#include "Adafruit_Thermal.h"
#include "modules/thermal_printer/thermal_printer.h"

// bmp images are converted to bitmap arrays using image_to_cpp script
#include "deco_top.h"
#include "deco_bottom.h"

Adafruit_Thermal printer(&Serial2);

namespace Phero {

	ThermalPrinter::ThermalPrinter(std::string module_id) : ModuleInterface(module_id) {
	}

	std::string ThermalPrinter::begin() {
		Serial2.begin(9600);
		// printer.begin();
		delay(3000);

		printer.justify('C');
		printer.setSize('L');
		printer.sleep();

		return "";
	}

	void ThermalPrinter::trigger(std::string payload) {
		printer.wake();

		// printer.printBitmap(deco_top_width, deco_top_height, deco_top_data);
		printer.println("~. ________ .~");
		printer.println(payload.c_str());
		printer.println("~. ________ .~");
		// printer.printBitmap(deco_bottom_width, deco_bottom_height, deco_bottom_data);

		printer.feed(2);
		printer.sleep();
	}

	std::string ThermalPrinter::checkState() {
		return "";
	}

}

#endif
