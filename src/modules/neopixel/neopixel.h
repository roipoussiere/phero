#pragma once

#include <Adafruit_NeoPixel.h>
#include "modules/module_interface.h"

#define DELIMITER " "
#define KEY_DELIMITER ":"


namespace Phero {

	class Neopixel : public ModuleInterface {

		public:
			Neopixel(std::string module_id, uint16_t nb_pixels, uint8_t pin, neoPixelType px_type = NEO_GRB + NEO_KHZ800);

			std::string begin();

			// Update leds state
			// Payload sample: "1:fff,2:fab400,5-8:00a"
			void trigger(std::string payload);
			std::string checkState();

			// Get the color of the given pixel (mainly used for test purposes)
			uint32_t getPixelColor(uint16_t pixel_pos);

		protected:
			uint8_t pin_;
			uint16_t nb_pixels_;
			neoPixelType px_type_;

			void setColorsFromPayload(std::string payload);
			void setColor(std::string data);
	};

}
