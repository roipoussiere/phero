#ifdef PHERO_NEOPIXEL

#include "modules/neopixel/neopixel.h"

Adafruit_NeoPixel pixels = Adafruit_NeoPixel();

namespace Phero {

	Neopixel::Neopixel(std::string module_id, uint16_t nb_pixels, uint8_t pin, neoPixelType px_type) : ModuleInterface(module_id) {
		this->nb_pixels_ = nb_pixels;
		this->pin_ = pin;
		this->px_type_ = px_type;
	}

	std::string Neopixel::begin() {
		pixels.updateLength(this->nb_pixels_);
		pixels.setPin(this->pin_);
		pixels.updateType(this->px_type_);

		pixels.begin();
		return "";
	}

	void Neopixel::trigger(std::string payload) {
		LOG(payload.c_str());
		this->setColorsFromPayload(payload);
		pixels.show();
	}

	std::string Neopixel::checkState() {
		return "";
	}

	uint32_t Neopixel::getPixelColor(uint16_t pixel_pos) {
		return pixels.getPixelColor(pixel_pos);
	}

	void Neopixel::setColorsFromPayload(std::string payload) {
		uint8_t start = 0;
		std::size_t end = payload.find(DELIMITER);

		if (payload == "") {
			pixels.clear();
			return;
		}

		while (end != std::string::npos) {
			this->setColor(payload.substr(start, end - start));
			start = end + strlen(DELIMITER);
			end = payload.find(DELIMITER, start);
		}

		this->setColor(payload.substr(start, end - start));
	}

	void Neopixel::setColor(std::string data) {
		std::size_t value_pos = data.find(KEY_DELIMITER);
		if (value_pos == std::string::npos) {
			// todo: mqtt warning
			LOG("no key delimiter found");
			return;
		}

		std::string value = data.substr(value_pos + strlen(KEY_DELIMITER), data.length());

		uint16_t pixel_pos;
		uint32_t pixel_color;

		if ( value.length() != 6 ) {
			// todo: mqtt warning
			LOG("pixel value: bad length");
			return;
		}

		try {
			pixel_pos = std::stoi(data.substr(0, value_pos));
		} catch(std::invalid_argument const& ex) {
			// todo: mqtt warning
			LOG("pixel pos: bad arg");
			return;
		}

		try {
			pixel_color = std::stoi(value, nullptr, 16);
		} catch(std::invalid_argument const& ex) {
			// todo: mqtt warning
			LOG("pixel color: bad arg");
			return;
		}

		pixels.setPixelColor(pixel_pos, pixel_color);
	}
}

#endif
