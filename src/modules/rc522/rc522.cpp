#ifdef PHERO_RC522

#include "modules/rc522/rc522.h"

namespace Phero {

	constexpr char hexmap[] = { '0', '1', '2', '3', '4', '5', '6', '7',
								'8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

	Rc522::Rc522(std::string module_id, uint8_t pin_sda, uint8_t pin_rst) : ModuleInterface(module_id) {
		this->rc522_ = MFRC522(pin_sda, pin_rst);
	}

	std::string Rc522::begin() {
		delay(2000);
		SPI.begin(); // TODO: one delay and one SPI begin for all modules
		this->rc522_.PCD_Init();
		return "";
	}

	void Rc522::trigger(std::string payload) {
		// TODO: send mqtt warning (rc522 has no trigger)
	}

	std::string Rc522::checkState() {
		std::string tag = "";

		this->rc522_.PICC_ReadCardSerial(); // must be called twice to avoid false negatives
		bool new_card_present = this->rc522_.PICC_IsNewCardPresent();
		bool read_card_serial = this->rc522_.PICC_ReadCardSerial();

		if (new_card_present && read_card_serial) {
			tag = hexStr(this->rc522_.uid.uidByte, rc522_.uid.size);
		}

		std::string state = "";
		if (tag != "" && this->last_tag_ == "") {
			state = std::string(MQTT_PAYLOAD_ENTER) + "-" + tag;
			this->last_tag_ = tag;
		} else if (tag == "" && this->last_tag_ != "") {
			state = std::string(MQTT_PAYLOAD_LEAVE) + "-" + this->last_tag_;
			this->last_tag_ = "";
		}
		return state;
	}

	std::string Rc522::hexStr(uint8_t *data, uint8_t len) {
		std::string str(len * 2, ' ');
		for (uint8_t i = 0; i < len; ++i) {
			str[2 * i] = hexmap[(data[i] & 0xF0) >> 4];
			str[2 * i + 1] = hexmap[data[i] & 0x0F];
		}
		return str;
	}

}

#endif
