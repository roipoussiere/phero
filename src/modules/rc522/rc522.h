#pragma once

#include <MFRC522.h>
#include "modules/module_interface.h"

#define MQTT_PAYLOAD_ENTER "en"
#define MQTT_PAYLOAD_LEAVE "le"
#define DELAY_AFTER_DETECTION 1000

namespace Phero {

	class Rc522 : public ModuleInterface {

		public:
			Rc522(std::string module_id, uint8_t pin_sda, uint8_t pin_rst);
			std::string begin();
			void trigger(std::string payload);
			std::string checkState();

		protected:
			MFRC522 rc522_;
			std::string last_tag_ = "";
			std::string hexStr(uint8_t *data, uint8_t len);

	};

}
