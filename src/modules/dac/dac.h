#pragma once

#include "modules/module_interface.h"
#include <driver/dac.h>

namespace Phero {

	class DAC : public ModuleInterface {

		public:
			DAC(std::string module_id, uint8_t pin_dac, uint8_t initial_value = 0);
			std::string begin();
			void trigger(std::string payload);
			std::string checkState();

		protected:
			uint8_t pin_dac_;
			uint8_t initial_value_;

	};

}
