#if (defined PHERO_DAC && defined TARGET_ESP32)

#include "modules/dac/dac.h"

namespace Phero {

	DAC::DAC(std::string module_id, uint8_t pin_dac, uint8_t initial_value) : ModuleInterface(module_id) {
		this->pin_dac_ = pin_dac;
		this->initial_value_ = initial_value;
	}

	std::string DAC::begin() {
		dac_output_enable(DAC_CHANNEL_1);
		dac_output_voltage(DAC_CHANNEL_1, this->initial_value_);
		return "";
	}

	void DAC::trigger(std::string payload) {
		uint8_t value;
		sscanf(payload.c_str(), "%d", &value);
		dac_output_voltage(DAC_CHANNEL_1, value);
	}

	std::string DAC::checkState() {
		return "";
	}

}

#endif
