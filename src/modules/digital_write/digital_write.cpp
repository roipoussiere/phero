#include "modules/digital_write/digital_write.h"


namespace Phero {

	DigitalWrite::DigitalWrite(std::string module_id, uint8_t pin_out, bool initial_state) : ModuleInterface(module_id) {
		this->pin_out_ = pin_out;
		this->initial_state_ = initial_state;
	}

	void DigitalWrite::init(std::string module_id, uint8_t pin_out, bool initial_state) {
		this->module_id_ = module_id;
		this->pin_out_ = pin_out;
		this->initial_state_ = initial_state;
	}

	std::string DigitalWrite::begin() {
		pinMode(pin_out_, OUTPUT);
		digitalWrite(this->pin_out_, this->initial_state_);
		return "";
	}

	void DigitalWrite::trigger(std::string payload) {
		if (payload == MQTT_PAYLOAD_HIGH) {
			digitalWrite(this->pin_out_, true);
		} else if (payload == MQTT_PAYLOAD_LOW) {
			digitalWrite(this->pin_out_, false);
		} else {
			// TODO: send mqtt warning (unrecognized payload)
		}
	}

	std::string DigitalWrite::checkState() {
		return "";
	}

}
