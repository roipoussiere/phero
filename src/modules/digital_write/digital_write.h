#pragma once

#include "modules/module_interface.h"

#define MQTT_PAYLOAD_HIGH "high"
#define MQTT_PAYLOAD_LOW  "low"


namespace Phero {

	class DigitalWrite : public ModuleInterface {

		public:
			DigitalWrite(std::string module_id = "unknown_d_write_module", uint8_t pin_out = 32, bool initial_state = LOW);
			void init(std::string module_id, uint8_t pin_out, bool initial_state = LOW);

			std::string begin();
			void trigger(std::string payload);
			std::string checkState();

		protected:
			uint8_t pin_out_;
			bool initial_state_;

	};

}
