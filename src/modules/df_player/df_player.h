#pragma once

#include <Arduino.h>
#include <stdexcept>

#include "DFRobotDFPlayerMini.h"

#include "modules/module_interface.h"
#include "phero_config.h"

#define SOFTWARE_SERIAL_BAUDS 9600


namespace Phero {

	class DfPlayer : public ModuleInterface {

		public:
			DfPlayer(std::string module_id, uint8_t volume);

			std::string begin();
			void trigger(std::string payload);
			std::string checkState();

		protected:
			uint8_t volume_; // from 0 to 30

			std::string getEventCode(uint8_t type, uint8_t value);
	};

}
