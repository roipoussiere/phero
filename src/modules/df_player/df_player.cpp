#ifdef PHERO_DFPLAYER

#include "modules/df_player/df_player.h"


DFRobotDFPlayerMini dfPlayer;

namespace Phero {

	DfPlayer::DfPlayer(std::string module_id, uint8_t volume): ModuleInterface(module_id) {
		this->volume_ = volume;
	}

	std::string DfPlayer::begin() {
		Serial2.begin(SOFTWARE_SERIAL_BAUDS);
		delay(10); // necessary to avoid issue when using Serial just after softwareSerial

		LOG("Initializing DFPlayer...");

		while( ! dfPlayer.begin(Serial2) ) {
			LOG("Unable to begin (check pins connection and sd card).");
			// TODO: send mqtt warning
			delay(1000);
		}
		LOG("DFPlayer ready.");
		
		dfPlayer.volume(this->volume_);
		return "";
	}

	void DfPlayer::trigger(std::string payload) {
		LOG(payload + ": ");
		if (payload[0] == 'p') {
			try {
				dfPlayer.play(std::stoi(payload.substr(1)));
			} catch(std::invalid_argument const& ex) {
				// todo: mqtt warning
				LOG("invalid track number");
				return;
			}
		} else if (payload[0] == 's') {
			dfPlayer.stop();
		} else {
			LOG("unrecognized command: " + payload);
		}

		if ( dfPlayer.available() ) {
			// TODO: send via mqtt instead
			LOG(getEventCode(dfPlayer.readType(), dfPlayer.read()));
		}
	}

	std::string DfPlayer::checkState() {
		return "";
	}

	std::string DfPlayer::getEventCode(uint8_t type, uint8_t value) {
		switch ( type ) {

			// *i*nformation events
			case DFPlayerCardInserted:
				return "ici";
			case DFPlayerCardRemoved:
				return "icr";
			case DFPlayerCardOnline:
				return "ico";
			case DFPlayerUSBInserted:
				return "iui";
			case DFPlayerUSBRemoved:
				return "iur";
			case DFPlayerPlayFinished:
				return "itf" + std::to_string(value);

			// *w*arning events
			case TimeOut:
				return "wto";
			case WrongStack:
				return "ws";
			case DFPlayerError:
				switch (value) {

				// *e*rror events
					case Busy:
						return "ecnf"; // card not found
					case Sleeping:
						return "es";
					case SerialWrongStack:
						return "ews";
					case CheckSumNotMatch:
						return "ewcs";
					case FileIndexOut:
						return "efio";
					case FileMismatch:
						return "efnf"; // file not found
					case Advertise:
						return "ea";
					default:
						return "eue"; // unknown error
				}
			default:
				return "eum"; // unknown message
		}
	}
}

#endif
