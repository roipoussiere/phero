#pragma once

#include <Arduino.h>
#include "modules/module_interface.h"

#define MQTT_PAYLOAD_HIGH "high"
#define MQTT_PAYLOAD_LOW  "low"
#define NB_STORED_VALUES 10


namespace Phero {

	class Hysteresis : public ModuleInterface {

		public:
			Hysteresis(std::string module_id, uint8_t pin_in, uint16_t low_threshold, uint16_t high_threshold);

			std::string begin();
			void trigger(std::string payload);
			std::string checkState();

		protected:
			uint8_t  step_;
			std::array<uint16_t, NB_STORED_VALUES> values_;
			uint8_t pin_in_;
			uint16_t low_threshold_;
			uint16_t high_threshold_;
			bool is_high_;

	};

}
