#include "modules/hysteresis/hysteresis.h"


namespace Phero {

	Hysteresis::Hysteresis(std::string module_id, uint8_t pin_in, uint16_t low_threshold, uint16_t high_threshold): ModuleInterface(module_id) {
		this->step_ = 0;
		this->pin_in_ = pin_in;
		this->values_.fill(0);
		this->low_threshold_ = low_threshold;
		this->high_threshold_ = high_threshold;
	}

	std::string Hysteresis::begin() {
		pinMode(this->pin_in_, INPUT);
		this->is_high_ = analogRead(this->pin_in_) > this->low_threshold_;
		return "";
	}

	void Hysteresis::trigger(std::string payload) {
		// TODO: send mqtt warning (pin_in has no trigger)
	}

	std::string Hysteresis::checkState() {
		this->values_[this->step_] = analogRead(this->pin_in_);

		uint16_t sum = 0;
		for(int i = 0 ; i<NB_STORED_VALUES ; i++) {
			sum += this->values_[i];
		}
		uint16_t average = sum / NB_STORED_VALUES;

		if ( ! this->is_high_ && average > this->high_threshold_) {
			this->is_high_ = HIGH;
			return MQTT_PAYLOAD_HIGH;
		} else if (this->is_high_ && average < this->low_threshold_) {
			this->is_high_ = LOW;
			return MQTT_PAYLOAD_LOW;
		}

		this->step_++;
		if (this->step_ >= NB_STORED_VALUES) {
			this->step_ = 0;
		}
		return "";
	}

}
