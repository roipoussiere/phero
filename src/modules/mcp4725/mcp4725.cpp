#ifdef PHERO_MCP4725

#include "modules/mcp4725/mcp4725.h"


Adafruit_MCP4725 dac;


namespace Phero {

	Mcp4725::Mcp4725(std::string module_id) : ModuleInterface(module_id) {
	}

	std::string Mcp4725::begin() {
		dac.begin(DAC_ADDRESS);
		dac.setVoltage(0, false);
		return "";
	}

	void Mcp4725::trigger(std::string payload) {
		if(payload[0] == 'H') {
			dac.setVoltage(MAX, false);
		}

		if(payload[0] == 'L') {
			dac.setVoltage(0, false);
		}

		if(payload[0] == 'h') {
			for (int i = MIN; i < MAX; i++) {
				dac.setVoltage(i, false);
				delay(DELAY);
			}
		}

		if(payload[0] == 'l') {
			for (int i = MAX; i > MIN; i--) {
				dac.setVoltage(i, false);
				Serial.println(i);
				delay(DELAY);
			}
		}

	}

	std::string Mcp4725::checkState() {
		return "";
	}

}

#endif
