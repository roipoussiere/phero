#pragma once

#include <Wire.h>
#include <Adafruit_MCP4725.h>
#include "modules/module_interface.h"
#include "phero_config.h"


#define DAC_ADDRESS 0x60
#define MIN 1450
#define MAX 1750
#define DELAY 40


namespace Phero {

	class Mcp4725 : public ModuleInterface {

		public:
			Mcp4725(std::string module_id);

			std::string begin();

			void trigger(std::string payload);

			std::string checkState();
	};

}
