#ifdef PHERO_74HC595

#include "modules/74hc595/74hc595.h"

#define DELIMITER ","

namespace Phero {

	SR74hc595::SR74hc595(std::string module_id, uint8_t register_size, uint8_t pin_ds, uint8_t pin_stcp, uint8_t pin_shcp) : ModuleInterface(module_id) {
		this->register_size_ = register_size;
		this->pin_ds_ = pin_ds;
		this->pin_stcp_ = pin_stcp;
		this->pin_shcp_ = pin_shcp;
	}

	std::string SR74hc595::begin() {
		pinMode(this->pin_ds_,   OUTPUT);
		pinMode(this->pin_stcp_, OUTPUT);
		pinMode(this->pin_shcp_, OUTPUT);

		this->pushRegister();
		return "";
	}

	void SR74hc595::trigger(std::string payload) {
		this->loadRegisterFromPayload(payload);
		// this->loadValue(payload);
		this->pushRegister();
	}

	std::string SR74hc595::checkState() {
		return "";
	}

	void SR74hc595::loadValue(std::string value) {
		uint8_t pin;

		LOG(value + ", ");
		if (sscanf(value.c_str(), "%d", &pin) == 1) {
			if (pin > 0) {
				LOG("values[" + std::to_string(pin - 1) + "] = hight");
				this->register_values[pin - 1] = HIGH;
			} else if (pin < 0) {
				LOG("values[" + std::to_string(- pin - 1) + "] = low");
				this->register_values[- pin - 1] = LOW;
			} else {
				// TODO: clean when 0
			}
		} else {
			LOG("warning: unrecognized value: " + value);
			// TODO: mqtt warning
		}

	}

	void SR74hc595::loadRegisterFromPayload(std::string payload) {
		// payload syntax: "-1,12,13,-15"
		// positive numbers sets to HIGH
		// negative numbers sets to LOW
		// index starts with 1
		// TODO: "5-8": set 5 to 8 to HIGH, "-1-16,5" set 1 to 16 to LOW, and 5 to HIGH
		uint8_t start = 0;
		std::size_t end = payload.find(DELIMITER);
		std::string item;

		LOG(payload + ": ");

		while (end != std::string::npos)
		{
			this->loadValue(payload.substr(start, end - start));
			start = end + strlen(DELIMITER);
			end = payload.find(DELIMITER, start);
		}

		this->loadValue(payload.substr(start, end - start));
	}

	void SR74hc595::pushRegister() {
		digitalWrite(this->pin_shcp_, LOW);

		for (uint8_t pin = (uint8_t(this->register_size_ / 8) + 1) * 8 - 1 ; pin >= 0 ; pin--) {
			digitalWrite(this->pin_stcp_, LOW);
			digitalWrite(this->pin_ds_, this->register_values[pin]);
			digitalWrite(this->pin_stcp_, HIGH);
		}

		digitalWrite(this->pin_shcp_, HIGH);
	}

}

#endif
