#pragma once

#include "modules/module_interface.h"
#include "phero_config.h"


namespace Phero {

	class SR74hc595 : public ModuleInterface {

		public:
			SR74hc595(std::string module_id, uint8_t register_size, uint8_t pin_ds, uint8_t pin_stcp, uint8_t pin_shcp);
			std::string begin();
			void trigger(std::string payload);
			std::string checkState();

		protected:
			uint8_t register_size_;
			bool register_values[64] = { LOW };
			uint8_t pin_ds_;
			uint8_t pin_stcp_;
			uint8_t pin_shcp_;
			void loadValue(std::string value);
			void loadRegisterFromPayload(std::string payload);
			void pushRegister();

	};

}
