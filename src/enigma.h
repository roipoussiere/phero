#pragma once

#include <string>
#include <map>
#include <initializer_list>
#include "modules/module_interface.h"

namespace Phero {

	class Enigma {

		public:
			template<class... Ts>
			Enigma(std::string enigma_id, Ts&&... modules) {
				this->enigma_id_ = enigma_id;
				std::initializer_list<ModuleInterface*> _modules = { &modules... };
				for(auto module : _modules) {
					this->addModule(*module);
				}
			}

			std::string getEnigmaId() {
				return this->enigma_id_;
			}

			ModuleInterface* getModule(std::string module_id) {
				return this->modules_[module_id];
			}

			std::map<std::string, ModuleInterface*> getModules() {
				return this->modules_;
			}

			void addModule(ModuleInterface& module) {
				module.setEnigma(this->enigma_id_);
				this->modules_[module.getModuleId()] = &module;
			}

		protected:
			std::string enigma_id_;
			std::map<std::string, ModuleInterface*> modules_;

	};

}
