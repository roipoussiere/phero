#include <Arduino.h>
#include <unity.h>
#include "SoftwareSerial.h"
#include "phero_config.h"

#define SOFT_SERIAL_BAUDS 9600


SoftwareSerial ss(32, 33);

void test_begin() {
	ss.begin(SOFT_SERIAL_BAUDS);
}

void test_print() {
	ss.println("hello");
}

void setup() {
	delay(2000); // required by unity test runner
	UNITY_BEGIN();

	RUN_TEST(test_begin);
	RUN_TEST(test_print);

	UNITY_END();
}

void loop() {}
