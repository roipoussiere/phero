#ifdef PHERO_NEOPIXEL

#include <unity.h>
#include "modules/neopixel/neopixel.h"
#include "phero_config.h"


Phero::Neopixel neopixel("test_neopixel", 10, 13);

void test_setup() {
	neopixel.begin();
	TEST_PASS();
}

void test_no_key_separator() {
	neopixel.trigger("1ff0000");
	delay(1000);
	TEST_PASS();
}

void test_bad_key() {
	neopixel.trigger("a:ff0000");
	delay(1000);
	TEST_PASS();
}

void test_bad_value() {
	neopixel.trigger("1:xxyyzz");
	delay(1000);
	TEST_PASS();
}

void test_bad_value_size() {
	neopixel.trigger("1:1234567");
	delay(1000);
	TEST_PASS();
}

void test_one_led() {
	neopixel.trigger("0:ff0000");
	delay(1000);
	TEST_PASS();
}

void test_one_led_short_form() {
	neopixel.trigger("1:00f");
	delay(1000);
	TEST_PASS();
}

void test_several_leds() {
	neopixel.trigger("2:ff0000,3:00ff00,4:0000ff");
	delay(1000);
	TEST_PASS();
}

void test_leds_range() {
	neopixel.trigger("5-8:ff0000");
	delay(1000);
	TEST_PASS();
}

void test_clear() {
	neopixel.trigger("");
	delay(1000);
	TEST_PASS();
}

void setup() {
	delay(2000); // required by unity test runner
	UNITY_BEGIN();

	RUN_TEST(test_setup);
	RUN_TEST(test_no_key_separator);
	RUN_TEST(test_bad_key);
	RUN_TEST(test_bad_value);
	RUN_TEST(test_bad_value_size);
	RUN_TEST(test_one_led);
	RUN_TEST(test_one_led_short_form);
	RUN_TEST(test_several_leds);
	RUN_TEST(test_leds_range);
	RUN_TEST(test_clear);

	UNITY_END();
}

void loop() {}

#endif
