#include <client.h>
#include <unity.h>


class TestDevice : public Phero::MqttClient {
	public:
		void onReceive(std::string topic, std::string payload) {}
		void onConnected() {}
};

TestDevice client;

void test_wifi_connection() {
	client.setupWifi();
	TEST_ASSERT(WiFi.status() == WL_CONNECTED);
}

void setup() {
	delay(2000);
	UNITY_BEGIN();
	Serial.begin(SERIAL_BAUDS);

	RUN_TEST(test_wifi_connection);

	UNITY_END();
}

void loop() {}
