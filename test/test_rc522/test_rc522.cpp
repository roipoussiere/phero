#ifdef PHERO_RC522

#include <unity.h>
#include <modules/rc522/rc522.h>
#include <phero_config.h>

#define LOOP_INTERVAL_MS 50
#define TIME_OUT 2000


Phero::Rc522 rc522("test_rc522", 22, 5);
unsigned long start = millis();

void test_enter() {
	TEST_ASSERT_TRUE_MESSAGE(true, "to be implented");
}

void test_leave() {
	TEST_ASSERT_TRUE_MESSAGE(true, "to be implented");
}

void setup() {
	delay(2000);
	rc522.begin();
	Serial.begin(SERIAL_BAUDS);
	Serial.println("Aproach a rfid tag close to the sensor to continue...");

	UNITY_BEGIN();
	RUN_TEST(test_enter);
	RUN_TEST(test_leave);
	UNITY_END();
}

void loop() {}

#endif
