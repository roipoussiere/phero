#include <Arduino.h>
#include <unity.h>
#include "pins.h"


uint8_t i = 0;

void test_pin_pulldown() {
	// INPUT_PULLDOWN is supported on esp32 only
	#ifdef TARGET_ESP32
	pinMode(pins[i], INPUT_PULLDOWN);

	TEST_ASSERT_FALSE_MESSAGE(digitalRead(pins[i]),
		("Pin " + String(pins[i]) + " should be down.").c_str());
	#endif
}

void test_pin_pullup() {
	pinMode(pins[i], INPUT_PULLUP);

	TEST_ASSERT_TRUE_MESSAGE(digitalRead(pins[i]),
		("Pin " + String(pins[i]) + " should be up.").c_str());
}

void test_pin_output() {
	pinMode(pins[i], OUTPUT);

	TEST_ASSERT_FALSE_MESSAGE(digitalRead(pins[i]),
		("Pin " + String(pins[i]) + " should be down.").c_str());
}

void test_pin_up() {
	digitalWrite(pins[i], HIGH);

	TEST_ASSERT_TRUE_MESSAGE(digitalRead(pins[i]),
		("Pin " + String(pins[i]) + " should be up.").c_str());
}

void test_pin_down() {
	digitalWrite(pins[i], false);

	TEST_ASSERT_FALSE_MESSAGE(digitalRead(pins[i]),
		("Pin " + String(pins[i]) + " should be down.").c_str());
}

void setup() {
	delay(2000); // wait until the test runner establishes serial connection
	UNITY_BEGIN();
}

void loop() {
	if (i == sizeof(pins) - 1) {
		UNITY_END();
	}

	RUN_TEST(test_pin_pulldown);
	RUN_TEST(test_pin_pullup);
	RUN_TEST(test_pin_output);
	RUN_TEST(test_pin_up);
	RUN_TEST(test_pin_down);

	i++;
}
