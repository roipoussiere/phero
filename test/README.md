# Phero unit tests

Running tests consist of 3 steps: build, upload and execution.

By default the `pio test` command processes all steps, but they can be disabled independently with `--without-building`, `--without-uploading` and `--without-testing` if necessary.

Default tests are used to work without any extra physical devices (like RFID reader, etc.), so you must define them explicitly.

Configuration files are located in `test/config` and are dedicated for a specific board: (`esp32.ini` and `esp01.ini`).

## Examples

Run all default tests for esp32 board:

    pio test -c test/config/esp32.ini

Run rc522 test (require rc522 module to be connected), very verbose mode:

    pio test -c test/config/esp32.ini -e rc522 -vv

Only build rc522 test (smoke test - do not require rc522 module to be connected):

    pio test -c test/config/esp32.ini --without-uploading --without-testing -e rc522

## Troubleshooting

- `Timed out waiting for packet header`:
  1. plug out an in usb cable;
  2. connect pin D2 to ground during flash, or move switch to PROG position.
- `invalid head of packet`:
  1. plug out an in usb cable;
  2. erase flash with `esptool.py erase_flash` and try again.
- upload step wait forever:
  1. try with an other usb port that can deliver more power (avoid usb switches).

## Debugging

To facilitate debugging, it's possible configure the monitor in order to convert the backtrace into file lines instead hexadecimal values:

    pio run -e dev
    pio device monitor -e dev --filter esp32_exception_decoder

You can also run the vscode monitor, which should read the `monitor_filters` in the platformio file.
