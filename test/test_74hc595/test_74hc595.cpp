#ifdef PHERO_74HC595

#include <unity.h>
#include <phero.h>

#define LOOP_INTERVAL_MS 50
#define TIME_OUT 2000


Phero::SR74hc595 sr_74hc595("test_74hc595", 64, 25, 32, 33);
unsigned long start = millis();

void test_enter() {
	TEST_ASSERT_TRUE_MESSAGE(true, "to be implented");
}

void test_leave() {
	TEST_ASSERT_TRUE_MESSAGE(true, "to be implented");
}

void setup() {
	delay(2000);
	sr_74hc595.begin();
	Serial.begin(SERIAL_BAUDS);

	UNITY_BEGIN();
	RUN_TEST(test_enter);
	RUN_TEST(test_leave);
	UNITY_END();
}

void loop() {}

#endif
