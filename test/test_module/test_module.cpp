#include <unity.h>
#include <phero.h>

#define PIN_TEST_IN  22
#define PIN_TEST_OUT 23
#define TIME_OUT 3


Phero::DigitalRead  d_read ("dr" , PIN_TEST_IN);
Phero::DigitalWrite d_write("dw", PIN_TEST_OUT);

Phero::Enigma hera("test_enigma", d_read, d_write);
Phero::Device device(hera);


void test_enigma_id() {
	TEST_ASSERT_EQUAL_STRING("test_enigma", device.getEnigma("test_enigma")->getEnigmaId().c_str());
}

void test_module_id() {
	TEST_ASSERT_EQUAL_STRING("dr", device.getModule("dr")->getModuleId().c_str());
	TEST_ASSERT_EQUAL_STRING("dw", device.getModule("dw")->getModuleId().c_str());
}

void test_module_query_topic() {
	TEST_ASSERT_EQUAL_STRING("mq/test_enigma/dr", device.getModule("dr")->getQueryTopic().c_str());
	TEST_ASSERT_EQUAL_STRING("mq/test_enigma/dw", device.getModule("dw")->getQueryTopic().c_str());
}

void test_module_response_topic() {
	TEST_ASSERT_EQUAL_STRING("mr/test_enigma/dr", device.getModule("dr")->getResponseTopic().c_str());
	TEST_ASSERT_EQUAL_STRING("mr/test_enigma/dw", device.getModule("dw")->getResponseTopic().c_str());
}

void test_module_info_topic() {
	TEST_ASSERT_EQUAL_STRING("mi/test_enigma/dr", device.getModule("dr")->getInfoTopic().c_str());
	TEST_ASSERT_EQUAL_STRING("mi/test_enigma/dw", device.getModule("dw")->getInfoTopic().c_str());
}

void test_module_warning_topic() {
	TEST_ASSERT_EQUAL_STRING("mw/test_enigma/dr", device.getModule("dr")->getWarningTopic().c_str());
	TEST_ASSERT_EQUAL_STRING("mw/test_enigma/dw", device.getModule("dw")->getWarningTopic().c_str());
}

void test_module_error_topic() {
	TEST_ASSERT_EQUAL_STRING("me/test_enigma/dr", device.getModule("dr" )->getErrorTopic().c_str());
	TEST_ASSERT_EQUAL_STRING("me/test_enigma/dw", device.getModule("dw")->getErrorTopic().c_str());
}

void test_begin() {
	device.begin(PHERO_CALLBACK(device));
	// TODO: get subscriptions - should be mq/test_enigma/dr and mq/test_enigma/dw
}

void test_module_query() {
	TEST_ASSERT_FALSE(digitalRead(PIN_TEST_OUT));
	device.publish("mq/test_enigma/dw", "up");

	uint16_t start = millis();
	while (millis() < start + TIME_OUT * 1000) {
		device.loop();
		if ( digitalRead(PIN_TEST_OUT) ) {
			TEST_PASS();
		}
	}

	TEST_FAIL_MESSAGE("message not received (time out)");
}

void test_module_info() {
	std::string topic;
	pinMode(PIN_TEST_IN, OUTPUT);
	digitalWrite(PIN_TEST_IN, HIGH);
	TEST_ASSERT_TRUE(digitalRead(PIN_TEST_IN));

	device.subscribe("mi/test_enigma/dr");

	digitalWrite(PIN_TEST_IN, LOW);

	uint start = millis();
	while (millis() < start + TIME_OUT * 1000) {
		device.loop();
		topic = device.getLastMqttTopic();
		if ( topic != "" && topic[1] != 'q') {
			TEST_ASSERT_EQUAL_STRING("mi/test_enigma/dr", topic.c_str());
			return;
		}
	}

	TEST_FAIL_MESSAGE("message not received (time out)");
}

void setup() {
	delay(2000); // required by unity test runner
	UNITY_BEGIN();

	RUN_TEST(test_enigma_id);
	RUN_TEST(test_module_id);

	RUN_TEST(test_module_query_topic);
	RUN_TEST(test_module_response_topic);
	RUN_TEST(test_module_info_topic);
	RUN_TEST(test_module_warning_topic);
	RUN_TEST(test_module_error_topic);

	RUN_TEST(test_begin);

	RUN_TEST(test_module_query);
	RUN_TEST(test_module_info);

	UNITY_END();
}

void loop() {}
