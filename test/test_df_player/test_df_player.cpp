#ifdef PHERO_DFPLAYER

#include <unity.h>
#include "modules/df_player/df_player.h"
#include "phero_config.h"


Phero::DfPlayer player("test_df_player", 32, 33, 10);

void test_begin() {
	player.begin();
	TEST_ASSERT_EQUAL_STRING("", player.checkState().c_str());
}

void test_play() {
	player.trigger("1");
}

void setup() {
	delay(2000); // required by unity test runner
	UNITY_BEGIN();

	RUN_TEST(test_begin);
	RUN_TEST(test_play);

	UNITY_END();
}

void loop() {}

#endif
