#include <unity.h>
#include "modules/digital_write/digital_write.h"
#include "phero_config.h"
#include "pins.h"

#define PIN_SINGLE     32


uint8_t i = 0;

std::array<Phero::DigitalWrite, sizeof(pins)> d_writes;
Phero::DigitalWrite single_d_write("test_d_write_single", PIN_SINGLE);

void test_single_begin() {
	single_d_write.begin();
	TEST_ASSERT_EQUAL_MESSAGE(LOW, digitalRead(PIN_SINGLE),
		("pin " + std::to_string(PIN_SINGLE) + " state should be LOW").c_str());
}

void test_single_raise() {
	TEST_ASSERT_EQUAL_MESSAGE(LOW, digitalRead(PIN_SINGLE),
		("pin " + std::to_string(PIN_SINGLE) + " state should be LOW").c_str());

	single_d_write.trigger("high");
	TEST_ASSERT_EQUAL_MESSAGE(HIGH, digitalRead(PIN_SINGLE),
		("pin " + std::to_string(PIN_SINGLE) + " state should be HIGH").c_str());
}

void test_single_fall() {
	TEST_ASSERT_EQUAL_MESSAGE(HIGH, digitalRead(PIN_SINGLE),
		("pin " + std::to_string(PIN_SINGLE) + " state should be HIGH").c_str());

	single_d_write.trigger("low");
	TEST_ASSERT_EQUAL_MESSAGE(LOW, digitalRead(PIN_SINGLE),
		("pin " + std::to_string(PIN_SINGLE) + " state should be LOW").c_str());
}

void test_init() {
	d_writes[i].init("test_d_read_" + std::to_string(pins[i]), pins[i]);
	TEST_PASS();
}

void test_begin() {
	d_writes[i].begin();
	TEST_ASSERT_EQUAL_MESSAGE(LOW, digitalRead(pins[i]),
		("pin " + std::to_string(pins[i]) + " state should be LOW").c_str());
}

void test_raise() {
	TEST_ASSERT_EQUAL_MESSAGE(LOW, digitalRead(pins[i]),
		("pin " + std::to_string(pins[i]) + " state should be LOW").c_str());

	d_writes[i].trigger("high");
	TEST_ASSERT_EQUAL_MESSAGE(HIGH, digitalRead(pins[i]),
		("pin " + std::to_string(pins[i]) + " state should be HIGH").c_str());
}

void test_fall() {
	TEST_ASSERT_EQUAL_MESSAGE(HIGH, digitalRead(pins[i]),
		("pin " + std::to_string(pins[i]) + " state should be HIGH").c_str());

	d_writes[i].trigger("low");
	TEST_ASSERT_EQUAL_MESSAGE(LOW, digitalRead(pins[i]),
		("pin " + std::to_string(pins[i]) + " state should be LOW").c_str());
}

void setup() {
	delay(2000); // required by unity test runner
	UNITY_BEGIN();

	RUN_TEST(test_single_begin);
	RUN_TEST(test_single_raise);
	RUN_TEST(test_single_fall);
}

void loop() {
	if (i == sizeof(pins) - 1) {
		UNITY_END();
	}

	RUN_TEST(test_init);
	RUN_TEST(test_begin);
	RUN_TEST(test_raise);
	RUN_TEST(test_fall);

	i++;
}
