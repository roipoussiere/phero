#include <unity.h>
#include <phero.h>

#define PIN_TEST_IN  22
#define PIN_TEST_OUT 23
#define TIME_OUT 3


class MyDevice : public Phero::Device {
	public:
		void onConnected() {
			this->subscribe("di/_test_device/+");
			this->subscribe("dw/_test_device/+");
			Phero::Device::onConnected();
		}

		void onReceive(std::string topic, std::string payload) {
			if (topic[1] == 'i') {
				TEST_ASSERT_EQUAL_STRING("di/_test_device/hi", topic.c_str());
			} else if (topic[1] == 'w') {
				TEST_ASSERT_EQUAL_STRING("dw/_test_device/ne", topic.c_str());
				TEST_PASS();
			} else {
				TEST_FAIL_MESSAGE(("received unexpected topic type: " + topic).c_str());
			}
			Phero::Device::onReceive(topic, payload);
		}
};

MyDevice device;


void test_device_name() {
	TEST_ASSERT_EQUAL_STRING("_test_device", device.getClientName().c_str());
}

void test_device_query_topic() {
	TEST_ASSERT_EQUAL_STRING("dq/_test_device/info", device.getQueryTopic("info").c_str());
}

void test_device_response_topic() {
	TEST_ASSERT_EQUAL_STRING("dr/_test_device/info", device.getResponseTopic("info").c_str());
}

void test_device_info_topic() {
	TEST_ASSERT_EQUAL_STRING("di/_test_device/hi", device.getInfoTopic("hi").c_str());
}

void test_device_warning_topic() {
	TEST_ASSERT_EQUAL_STRING("dw/_test_device/ne", device.getWarningTopic("ne").c_str());
}

void test_device_error_topic() {
	TEST_ASSERT_EQUAL_STRING("de/_test_device/bad", device.getErrorTopic("bad").c_str());
}

void test_info_and_warning() {
	device.begin(PHERO_CALLBACK(device));

	uint16_t start = millis();
	while (millis() < start + TIME_OUT * 1000) {
		device.loop();
	}

	// device.onReceive() checks info and warning and should finish the test
	TEST_FAIL_MESSAGE("message not received (time out)");
}

void setup() {
	delay(2000); // required by unity test runner
	UNITY_BEGIN();

	RUN_TEST(test_device_name);

	RUN_TEST(test_device_query_topic);
	RUN_TEST(test_device_response_topic);
	RUN_TEST(test_device_info_topic);
	RUN_TEST(test_device_warning_topic);
	RUN_TEST(test_device_error_topic);

	RUN_TEST(test_info_and_warning);

	UNITY_END();
}

void loop() {}
