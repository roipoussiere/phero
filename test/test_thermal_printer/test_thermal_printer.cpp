#ifdef PHERO_THERMAL_PRINTER

#include <unity.h>
#include <phero.h>
#include <phero_config.h>

#define LOOP_INTERVAL_MS 50
#define TIME_OUT 2000


Phero::ThermalPrinter printer("test_printer");
unsigned long start = millis();

void test_print() {
	TEST_ASSERT_TRUE_MESSAGE(true, "to be implented");
}

void setup() {
	delay(2000);
	printer.begin();
	Serial.begin(SERIAL_BAUDS);

	UNITY_BEGIN();
	RUN_TEST(test_enter);
	UNITY_END();
}

void loop() {}

#endif
