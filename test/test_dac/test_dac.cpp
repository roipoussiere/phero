#ifdef PHERO_DAC

#include <unity.h>
#include <phero.h

Device device = Device("TestAnalog");
ModuleDAC dac = ModuleDAC("dac", 25, 0);

void setup() {
	device.addEnigma("test", dac);
	device.start(PHERO_CALLBACK(device));
}

void loop() {
	device.loop();
}

#endif
