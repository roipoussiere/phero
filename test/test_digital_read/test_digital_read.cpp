#include <unity.h>
#include "modules/digital_read/digital_read.h"
#include "phero_config.h"
#include "pins.h"

#define PIN_SINGLE     32


uint8_t i = 0;

std::array<Phero::DigitalRead, sizeof(pins)> d_writes;
Phero::DigitalRead single_d_read("test_d_read_single", PIN_SINGLE, OUTPUT);

void test_single_begin() {
	single_d_read.begin();
	TEST_ASSERT_EQUAL_STRING_MESSAGE("", single_d_read.checkState().c_str(),
		("pin " + std::to_string(PIN_SINGLE) + " state should be an empty string").c_str());
}

void test_single_high() {
	digitalWrite(PIN_SINGLE, HIGH);

	TEST_ASSERT_EQUAL_STRING_MESSAGE("high", single_d_read.checkState().c_str(),
		("pin " + std::to_string(PIN_SINGLE) + " state should be high").c_str());
	TEST_ASSERT_EQUAL_STRING_MESSAGE("",    single_d_read.checkState().c_str(),
		("pin " + std::to_string(PIN_SINGLE) + " state should be an empty string").c_str());
}

void test_single_low() {
	digitalWrite(PIN_SINGLE, LOW);

	TEST_ASSERT_EQUAL_STRING_MESSAGE("low", single_d_read.checkState().c_str(),
		("pin " + std::to_string(PIN_SINGLE) + " state should be low").c_str());
	TEST_ASSERT_EQUAL_STRING_MESSAGE("",    single_d_read.checkState().c_str(),
		("pin " + std::to_string(PIN_SINGLE) + " state should be an empty string").c_str());
}

void test_init() {
	d_writes[i].init("test_d_read_" + std::to_string(pins[i]), pins[i], OUTPUT);
	TEST_PASS();
}

void test_begin() {
	d_writes[i].begin();

	TEST_ASSERT_EQUAL_STRING_MESSAGE("", d_writes[i].checkState().c_str(),
		("pin " + std::to_string(pins[i]) + " state should be an empty string").c_str());
}

void test_high() {
	digitalWrite(pins[i], HIGH);

	TEST_ASSERT_EQUAL_STRING_MESSAGE("high", d_writes[i].checkState().c_str(),
		("pin " + std::to_string(pins[i]) + " state should be high").c_str());
	TEST_ASSERT_EQUAL_STRING_MESSAGE("",    d_writes[i].checkState().c_str(),
		("pin " + std::to_string(pins[i]) + " state should be an empty string").c_str());
}

void test_low() {
	digitalWrite(pins[i], LOW);

	TEST_ASSERT_EQUAL_STRING_MESSAGE("low", d_writes[i].checkState().c_str(),
		("pin " + std::to_string(pins[i]) + " state should be low").c_str());
	TEST_ASSERT_EQUAL_STRING_MESSAGE("",    d_writes[i].checkState().c_str(),
		("pin " + std::to_string(pins[i]) + " state should be an empty string").c_str());
}

void setup() {
	delay(2000); // required by unity test runner
	UNITY_BEGIN();
	RUN_TEST(test_single_begin);
	RUN_TEST(test_single_high);
	RUN_TEST(test_single_low);
}

void loop() {
	if (i == sizeof(pins) - 1) {
		UNITY_END();
	}

	RUN_TEST(test_init);
	RUN_TEST(test_begin);
	RUN_TEST(test_high);
	RUN_TEST(test_low);

	i++;
}
