#include <unity.h>
#include "enigma.h"
#include "modules/digital_read/digital_read.h"
#include "modules/digital_write/digital_write.h"

#define TEST_PIN 23


Phero::Enigma enigma1("test_enigma1");
Phero::DigitalRead in1("in1", TEST_PIN);
Phero::DigitalRead out1("out1", TEST_PIN);

Phero::Enigma enigma2("test_enigma2", in1, out1);

Phero::DigitalRead in3("in3", TEST_PIN);
Phero::DigitalRead out3("out3", TEST_PIN);
Phero::Enigma enigma3("test_enigma3", in3, out3);

Phero::Enigma enigma4("test_enigma4",
	Phero::DigitalRead("in4" , TEST_PIN),
	Phero::DigitalRead("out4", TEST_PIN)
);


void test_enigma_id() {
	TEST_ASSERT_EQUAL_STRING("test_enigma1", enigma1.getEnigmaId().c_str());
	TEST_ASSERT_EQUAL_STRING("test_enigma2", enigma2.getEnigmaId().c_str());
	TEST_ASSERT_EQUAL_STRING("test_enigma3", enigma3.getEnigmaId().c_str());
}

void test_add_in() {
	enigma1.addModule(in1);

	Phero::ModuleInterface* module1_in = enigma1.getModule("in1");
	TEST_ASSERT_EQUAL_STRING("in1", enigma1.getModules()["in1"]->getModuleId().c_str());
	TEST_ASSERT_EQUAL_STRING("in1", module1_in->getModuleId().c_str());
	TEST_ASSERT_EQUAL_STRING("test_enigma1", module1_in->getEnigmaId().c_str());
}

void test_add_out() {
	enigma1.addModule(out1);

	Phero::ModuleInterface* module1_out = enigma1.getModule("out1");
	TEST_ASSERT_EQUAL_STRING("test_enigma1", module1_out->getEnigmaId().c_str());
	TEST_ASSERT_EQUAL_STRING("out1", module1_out->getModuleId().c_str());
}

void test_inline_used() {
	Phero::ModuleInterface* module2_in = enigma2.getModule("in1");
	TEST_ASSERT_EQUAL_STRING("test_enigma2", module2_in->getEnigmaId().c_str());
	TEST_ASSERT_EQUAL_STRING("in1", module2_in->getModuleId().c_str());

	Phero::ModuleInterface* module2_out = enigma2.getModule("out1");
	TEST_ASSERT_EQUAL_STRING("test_enigma2", module2_out->getEnigmaId().c_str());
	TEST_ASSERT_EQUAL_STRING("out1", module2_out->getModuleId().c_str());
}

void test_inline() {
	Phero::ModuleInterface* module3_in = enigma3.getModule("in3");
	TEST_ASSERT_EQUAL_STRING("test_enigma3", module3_in->getEnigmaId().c_str());
	TEST_ASSERT_EQUAL_STRING("in3", module3_in->getModuleId().c_str());

	Phero::ModuleInterface* module3_out = enigma3.getModule("out3");
	TEST_ASSERT_EQUAL_STRING("test_enigma3", module3_out->getEnigmaId().c_str());
	TEST_ASSERT_EQUAL_STRING("out3", module3_out->getModuleId().c_str());
}

void test_inline_declare() {
	Phero::ModuleInterface* module3_in = enigma3.getModule("in3");
	TEST_ASSERT_EQUAL_STRING("test_enigma3", module3_in->getEnigmaId().c_str());
	TEST_ASSERT_EQUAL_STRING("in3", module3_in->getModuleId().c_str());

	Phero::ModuleInterface* module3_out = enigma3.getModule("out3");
	TEST_ASSERT_EQUAL_STRING("test_enigma3", module3_out->getEnigmaId().c_str());
	TEST_ASSERT_EQUAL_STRING("out3", module3_out->getModuleId().c_str());
}

void setup() {
	delay(2000); // required by unity test runner
	UNITY_BEGIN();

	RUN_TEST(test_enigma_id);
	RUN_TEST(test_add_in);
	RUN_TEST(test_add_out);
	RUN_TEST(test_inline_used); // fails ('test_enigma1' instead 'test_enigma2')
	RUN_TEST(test_inline);
	// RUN_TEST(test_inline_declare); // crash

	UNITY_END();
}

void loop() {}
