#include <client.h>
#include <unity.h>
#include <string>

#define MQTT_TOPIC_PING "ping"
#define MQTT_COMMON_PAYLOAD "-"
#define TIME_OUT 3


class MyMqttClient : public Phero::MqttClient {
	public:
		void onConnected() {
			TEST_PASS();
		}

		void onReceive(std::string topic, std::string payload) {
			if (topic == MQTT_TOPIC_PING) {
				TEST_PASS();
			} else {
				TEST_FAIL_MESSAGE(("received wrong topic: " + topic).c_str());
			}
		}
};

MyMqttClient client;

void test_mqtt_connection() {
	for(uint8_t i=0 ; i<3 ; i++) {
		LOG("=== mqtt connection test n°" + std::to_string(i + 1) + " ===");
		client.begin(PHERO_CALLBACK(client));
	}
	TEST_FAIL_MESSAGE("Not connected.");
}

void test_mqtt_ping() {
	client.subscribe(MQTT_TOPIC_PING);
	client.publish(MQTT_TOPIC_PING, MQTT_COMMON_PAYLOAD);
	uint start = millis();
	while (millis() < start + TIME_OUT * 1000) {
		client.loop();
	}
	TEST_FAIL_MESSAGE("message not received (time out)");
}

void setup() {
	delay(2000);
	UNITY_BEGIN();
	RUN_TEST(test_mqtt_connection);
	RUN_TEST(test_mqtt_ping);
	UNITY_END();
}

void loop() {}
