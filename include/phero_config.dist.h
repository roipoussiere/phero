// Baud rate for serial communication.
#define SERIAL_BAUDS 115200

// The SSID of the local wifi network on which devices connect
#define WIFI_SSID "My wifi SSID"

// The password of the local wifi network on which devices connect
#define WIFI_PASSWORD "My wifi password"

// IP address or host name of the MQTT broker
#define BROKER_HOST "192.168.0.42"

// Network port of the MQTT broker
#define BROKER_PORT 1883
